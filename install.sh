#!/bin/bash

SCRIPTDIR=$(cd "$( dirname "$0" )" && pwd)
DOTFILES=$SCRIPTDIR/files
PWD=$(pwd)

SAFE_LINK=true

while getopts "h?vf" opt; do
    case "$opt" in
    h|\?)
        echo "help!"
        exit 0
        ;;
    f)  SAFE_LINK=false
        ;;
    esac
done

link() {
    local src="$1"
    local dest="$HOME/`basename $src`"

    if [ "$SAFE_LINK" = true ]; then
        [ ! -e "$dest" ] && ln -sfv "$src" "$dest"
    else
        ln -sfv "$src" "$dest"
    fi

}

for FILE in $(ls -A $DOTFILES)
do
    link $DOTFILES/$FILE
done
